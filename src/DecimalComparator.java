public class DecimalComparator {
    /*
        Write a method areEqualByThreeDecimalPlaces with two parameters of type double.
        The method should return boolean and it needs to return true if two double numbers are the same up to three decimal places. Otherwise, return false.
     */
    public static boolean areEqualByThreeDecimalPlaces(double input1, double input2){
        int val1 = (int) (input1 * 1000);
        int val2 = (int) (input2 * 1000);
        if(val1 == val2)
        {
            return true;
        }
        return false;
    }

    public static void main (String args[]){
        System.out.println(areEqualByThreeDecimalPlaces(3.33,3.33));
    }
}

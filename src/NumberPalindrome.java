public class NumberPalindrome {
    /*  Write a method called isPalindrome with one int parameter called number.
        The method needs to return a boolean.
        It should return true if the number is a palindrome number otherwise it should return false.

     */
    public static boolean isPalindrome(int number){
        int temp = number;
        int reverse = 0;
        while(temp != 0){
            reverse = reverse * 10;
            reverse += temp % 10;
            temp = temp/10;
        }
        if(number == reverse){
            return true;
        }

        return false;
    }

    public static void main (String args[]){
        System.out.println(isPalindrome(-222));

    }

}

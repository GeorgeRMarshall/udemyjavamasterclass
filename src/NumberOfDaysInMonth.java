public class NumberOfDaysInMonth {
    /*
    Write a method isLeapYear with a parameter of type int named year.
    The parameter needs to be greater than or equal to 1 and less than or equal to 9999.
    If the parameter is not in that range return false.
    Otherwise, if it is in the valid range, calculate if the year is a leap year and return true if it is, otherwise return false.
    A year is a leap year if it is divisible by 4 but not by 100, or it is divisible by 400.
     */

    /*
    Write another method getDaysInMonth with two parameters month and year.  ​Both of type int.
    If parameter month is < 1 or > 12 return -1. ​
    If parameter year is < 1 or > 9999 then return -1.
    This method needs to return the number of days in the month. Be careful about leap years they have 29 days in month 2 (February).
    You should check if the year is a leap year using the method isLeapYear described above.
     */

    public static boolean isLeapYear(int year){
        if(year > 0 && year < 10000) {
            if((year%4) == 0 && (year%100) != 0){
                return true;
            }else if((year % 400) == 0){
                return true;
            }
        }
        return false;
    }

    public static int getDaysInMonth(int month, int year){
        if(month<1 || month > 12){
            return -1;
        }

        if(year < 1 || year > 9999){
            return -1;
        }
        int val = 0;
        switch (month){
            case 1:
                val = 31;
                break;
            case 2:
                if(isLeapYear(year) == true){
                    val = 29;
                }else{
                    val = 28;
                }

                break;
            case 3:
                val = 31;
                break;
            case 4:
                val = 30;
            break;
            case 5:
                val =31;
            break;
            case 6:
                val = 30;
            break;
            case 7:
                val = 31;
            break;
            case 8:
                val = 31;
            break;
            case 9:
                val = 30;
            break;
            case 10:
                val = 31;
            break;
            case 11:
                val = 30;
            break;
            case 12:
                val = 31;
            break;
        }

        return val;
    }



    public static void main (String args[]){
        System.out.println(isLeapYear(2020));
        System.out.println(getDaysInMonth(2, 2020));

    }
}

import java.util.*;
public class L20PoundToKG {
    public static void main(String[] args){
        //Convert KG to pounds given that one KG is equal to 0.45359237 pounds
        Scanner scan = new Scanner(System.in);
        double pounds = scan.nextDouble();
        double kg = pounds * 0.45359237d;
        System.out.println("Pounds: " + pounds);
        System.out.println("KGs: " + kg);

    }

}

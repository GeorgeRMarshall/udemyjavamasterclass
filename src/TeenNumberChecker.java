public class TeenNumberChecker {
    /*
        We'll say that a number is "teen" if it is in the range 13 -19 (inclusive).
        Write a method named hasTeen with 3 parameters of type int.
        The method should return boolean and it needs to return true if one of the parameters is in range 13(inclusive) - 19 (inclusive). Otherwise return false.

        Write another method named isTeen with 1 parameter of type int.
        The method should return boolean and it needs to return true if the parameter is in range 13(inclusive) - 19 (inclusive). Otherwise return false.

     */
    public static boolean hasTeen(int age1, int age2, int age3){
        if((age1 > 12 && age1 < 20) || (age2 > 12 && age2 < 20) || (age3 > 12 && age3 < 20))
        {
            return true;
        }
        return false;
    }

    public static boolean isTeen(int input){
        if(input > 12 && input < 20)
        {
            return true;
        }
        return false;
    }
    public static void main (String args[]){
        System.out.println(hasTeen(13,20,99));
        System.out.println(isTeen(13));
    }

}

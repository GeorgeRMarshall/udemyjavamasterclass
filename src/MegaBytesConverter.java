public class MegaBytesConverter {
    /*
    Converts kilobytes to megabytes and remaining kilobytes
     */
    public static void printMegaBytesAndKiloBytes(int kiloBytes){
        if(kiloBytes < 0){
            System.out.println("Invalid Value");
            return;
        }
        int megabytes = 0;
        int kilobytesRemainder = 0;
        megabytes = kiloBytes/1024;
        kilobytesRemainder = kiloBytes % 1024;
        System.out.println(kiloBytes + " KB = " + megabytes + " MB and " + kilobytesRemainder + " KB");

    }

    public static void main (String args[])
    {
        printMegaBytesAndKiloBytes(2500);
    }
}

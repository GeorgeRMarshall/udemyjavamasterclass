public class operatorsExercise {
    public static void main(String[] args){
        //Exercise Explained
        // 1. Create double with value 20
        // 2. Create double with value 80
        // 3. Add them together and multiple result by 25
        // 4. Modulus the result by 40
        // 5. If the result is less or equal to 20 print "Total was over the limit"
        double var1 = 20;
        double var2 = 80;
        double var3 = (var1 + var2) * 25;
        double result = var3 % 40;
        if(result <= 20)
        {
            System.out.println("Total was over the limit");
        }

    }
}

public class SpeedConverter {
    /*
    Converts KPH to MPH and prints out the result
     */
    public static long toMilesPerHour(double kphInput){
        if(kphInput < 0)
        {
            return -1;
        }else
        {
            long mphOutput = Math.round(kphInput/ 1.609);
            return mphOutput;
        }
    }

    public static void printConversion(double kphInput){
        if(kphInput < 0)
        {
            System.out.println("Invalid Value");
        }else
        {
            System.out.println(kphInput + " Km/h = " + toMilesPerHour(kphInput) + " MPH");
        }
    }

    public static void main (String args[])
    {
        System.out.println(toMilesPerHour(-5));
        printConversion(10);
    }
}
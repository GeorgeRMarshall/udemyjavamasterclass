public class EqualSumChecker {
    /*
        Write a method hasEqualSum with 3 parameters of type int.
        The method should return boolean and it needs to return true if the sum of the first and second parameter is equal to the third parameter. Otherwise, return false.
     */
        public static boolean hasEqualSum(int input1, int input2, int output){
            if((input1 + input2)==output)
            {
                return true;
            }

            return false;
        }

        public static void main (String args[]){
            System.out.println(hasEqualSum(1,1,2));
        }
}

public class BarkingDog {
    /*
    If a dog is barking between 22 and 8 the method returns wake up(true)
 */
    public static boolean shouldWakeUp(boolean barking, int hourOfDay){

        if(hourOfDay < 0 || hourOfDay > 23)
        {
            return false;
        }
        if(barking == true && hourOfDay < 8 || hourOfDay > 22)
        {
            return true;

        }else{
            return false;
        }

    }

    public static void main (String args[]){
        System.out.println(shouldWakeUp(true, 23));
    }
}

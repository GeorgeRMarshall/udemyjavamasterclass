public class Sum3and5 {
    /*
    Create a for statement usung a range of numbers from 1 to 1000 inclusive
    Sum all the numbers that can be divided with 3 and 5
    break out of the loo[ once five numbers meet the conditions
    print the sum of the numbers

     */

    public static void main (String args[]){
        int sum = 0;
        int count = 0;
        for(int i = 1; i <= 1000; i++)
        {

            if((i%3 == 0) && (i%5==0)){
                sum += i;
                count++;
                System.out.println("Found Number: "+ i);

            }
            if(count == 5){
                break;
            }
        }
        System.out.println("Sum: " + sum);
    }
}
